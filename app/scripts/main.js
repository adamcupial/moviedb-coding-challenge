/* jshint esversion: 6 */

(function (mDb) {
    'use strict';

    /**
     * Wrapper around console.log/error etc. functions
     *
     * @method logger
     * @param {String} type
     * @private
     * @return {Void}
     **/
    function logger (type) {
        var args = Array.prototype.slice.call(arguments, 1);

        if (window.console && window.console[type] && args.length) {
            window.console[type].apply(window.console, args);
        }
    }

    /**
     * Helper to convert strange array notation into normal object
     *
     * @method arrayToObj
     * @param {Array} arr
     * @private
     * @return {Object}
     **/
    function arrayToObj (arr) {
        var obj = {};

        arr.forEach(function (arrObj) {
            obj[arrObj.name] = arrObj.value;
        });

        return obj;
    }

    /**
     * @class App
     **/
    function App () {
        this.apiKey = 'dd19df850d88eaf34836bac4cf729c4e';
        return this.init();
    }

    App.prototype = Object.create(null, {
        /**
         * @method App.getImageUrl
         * @return {String} url
         * @private
         **/
        getImageUrl: {
            value: function (path) {
                return `http://image.tmdb.org/t/p/w185${path}`;
            },
        },
        /**
         * Wrapper around themoviejs library - get movie by id
         *
         * @method App.getDbMovie
         * @param {String} id
         * @return {jQuery.Deferred}
         **/
        getDbMovie: {
            value: function (id) {
                var $defer = $.Deferred();

                mDb.movies.getById({id: id, }, function (data) {
                    $defer.resolve(data);
                }, function (error) {
                    $defer.reject(error);
                });

                return $defer;
            },
        },
        /**
         * Wrapper around themoviejs library - get tv series by id
         *
         * @method App.getDbTV
         * @param {String} id
         * @return {jQuery.Deferred}
         **/
        getDbTV: {
            value: function (id) {
                var $defer = $.Deferred();

                mDb.tv.getById({id: id, }, function (data) {
                    $defer.resolve(data);
                }, function (error) {
                    $defer.reject(error);
                });

                return $defer;
            },
        },
        /**
         * Wrapper around themoviejs library - search movies
         *
         * @method App.searchDbMovie
         * @param {String} query
         * @param {Object} [options]
         * @param {Number} [options.page=1]
         * @return {jQuery.Deferred}
         **/
        searchDbMovie: {
            value: function (query, options) {
                var $defer = $.Deferred(),
                    page = options.page || 1;

                mDb.search.getMovie({
                    query: encodeURI(query),
                    page: page,
                }, function (data) {
                    $defer.resolve(data);
                }, function (data) {
                    $defer.reject(data);
                });

                return $defer;
            },
        },
        /**
         * Wrapper around themoviejs library - search tv series
         *
         * @method App.searchDbTV
         * @param {String} query
         * @param {Object} [options]
         * @param {Number} [options.page=1]
         * @return {jQuery.Deferred}
         **/
        searchDbTV: {
            value: function (query, options) {
                var $defer = $.Deferred(),
                    page = options.page || 1;

                mDb.search.getTv({
                    query: encodeURI(query),
                    page: page,
                }, function (data) {
                    $defer.resolve(data);
                }, function (data) {
                    $defer.reject(data);
                });

                return $defer;
            },
        },
        /**
         * Wrapper around themoviejs library - search tv series AND movies
         *
         * @method App.searchDb
         * @param {String} query
         * @param {Object} [options]
         * @param {Number} [options.page=1]
         * @return {jQuery.Deferred}
         **/
        searchDb: {
            value: function (query, options) {
                var $defer = $.Deferred();

                if (options.type === 'movie') {
                    this.searchDbMovie(query, options)
                        .done(function (data) {
                            $defer.resolve({
                                movie: data,
                                tv: [],
                            });
                        })
                        .fail(function (error) {
                            $defer.reject(error);
                        });
                } else if (options.type === 'tv') {
                    this.searchDbTV(query, options)
                        .done(function (data) {
                            $defer.resolve({
                                tv: data,
                                movie: [],
                            });
                        })
                        .fail(function (error) {
                            $defer.reject(error);
                        });
                } else {
                    $.when(this.searchDbTV(query, options), this.searchDbMovie(query, options))
                        .done(function (dataTv, dataMovie) {
                            $defer.resolve({
                                tv: dataTv,
                                movie: dataMovie,
                            });
                        })
                        .fail(function (error) {
                            $defer.reject(error);
                        });
                }
                return $defer;
            },
        },
        /**
         * method to prepare detail page
         * @method App.renderDetail
         * @param {String} data - json decoded data
         **/
        renderDetail: {
            value: function (data) {
                var $detail = $('.detail'),
                    content = '',
                    toIgnore = [
                        'backdrop_path',
                        'belongs_to_collection',
                        'id',
                        'seasons',
                        'poster_path',
                        'video',
                    ];

                data = JSON.parse(data);

                if (!$detail.length) {
                    this.$main.after('<div class="detail"></div>');
                    $detail = $('.detail');
                }

                Object.keys(data).filter(function (key) {
                    return toIgnore.indexOf(key) === -1 && data[key];
                }).forEach(function (key) {
                    var stringified = key.replace(/_/g, ' '),
                        value = data[key];

                    if (typeof(value) === 'object' && value.length && value[0].name) {
                        value = value.reduce(function (prev, next) {
                            return prev + ', ' + next.name;
                        }, '');
                    }

                    if (typeof(value) === 'string' && value.indexOf('http') === 0) {
                        value = `<a href="${value}" target="_blank">${value}</a>`;
                    }

                    content += `<div>
                            <strong>${stringified}:</strong>
                            <span>${value}</span>
                        </div>`;
                });

                window.requestAnimationFrame(function () {
                    $detail.html(content);
                });
            },
        },
        /**
         * method to get item data and render detail page
         *
         * @method App.showDetail
         * @param {Number} id
         * @param {String} type
         **/
        showDetail: {
            value: function (id, type) {
                var that = this;

                if (type === 'series') {
                    this.getDbTV(id).
                        done(function (data) {
                            that.renderDetail(data);
                        });
                } else {
                    this.getDbMovie(id).
                        done(function (data) {
                            that.renderDetail(data);
                        });
                }
            },
        },
        /**
         * initializer
         *
         * @method App.init
         **/
        init: {
            value: function () {
                var that = this;

                logger('log', 'Application initialized');
                mDb.common.api_key = this.apiKey;

                this.$form = $('form');
                this.$form.on('submit', this.formSubmit.bind(this));
                this.$main = $('main');

                this.$main.on('click', '.next, .prev', function () {
                    var data = arrayToObj(that.$form.serializeArray());
                    that.search(data.q, $.extend({}, data, {page: $(this).attr('data-page'), }));
                });

                this.$main.on('click', 'article', function () {
                    that.showDetail($(this).attr('data-id'), $(this).attr('data-type'));
                });

                $('body').on('click', '.detail', function () {
                    $('.detail').html('');
                });

                return this;
            },
        },
        /**
         * search and display the results
         *
         * @method App.search
         * @param {String} query
         * @param {Object} [options]
         **/
        search: {
            value: function (query, options) {
                var that = this;

                this.searchDb(query, options).done(function (response) {
                    var tv = response.tv,
                        movie = response.movie;

                    if (tv.length) {
                        tv = JSON.parse(tv);
                    } else {
                        tv = {
                            page: 1,
                            results: [],
                            total_results: 0,
                            total_pages: 1,
                        };
                    }

                    if (movie.length) {
                        movie = JSON.parse(movie);
                    } else {
                        movie = {
                            page: 1,
                            results: [],
                            total_results: 0,
                            total_pages: 1,
                        };
                    }
                    that.populateLists(tv, movie);
                });
            },
        },
        /**
         * form submit handler
         * @method App.formSubmit
         * @private
         * @param {Event} ev
         **/
        formSubmit: {
            value: function (ev) {
                var data = arrayToObj(this.$form.serializeArray());

                ev.preventDefault();
                this.search(data.q, data);
            },
        },
        /**
         * render single item html
         * @method App.renderListItem
         * @param {Object} item - single data item
         **/
        renderListItem: {
            value: function (item) {
                var img = '',
                    year = 'unknown';

                if (item.poster_path) {
                    img = `<img src="${this.getImageUrl(item.poster_path)}" alt="${item.name || item.title}" />`;
                }

                if (item.first_air_date) {
                    year = item.first_air_date.slice(0, 4);
                }

                if (item.release_date) {
                    year = item.release_date.slice(0, 4);
                }

                return `<article class="list-item" data-id="${item.id}" data-type="${item.name ? 'series' : 'movie'}">
                    <header><h3>${item.name ? 'Series:' : 'Movie:'} ${item.name || item.title} (${year})</h3></header>
                    <div class="body">
                        ${img}
                        <div class="overview"> ${item.overview} </div>
                    </div>
                    <footer>
                        <span class="vote">${item.vote_average}/10</span>
                    </footer>
                </article>`;
            },
        },
        /**
         * push results as html to page
         * @method populateLists
         * @param {Object} tvList - result from moviedb
         * @param {Object} movieList - result from moviedb
         **/
        populateLists: {
            value: function (tvList, movieList) {
                var renderedTv,
                    renderedMovie,
                    paginator = '',
                    that = this;

                renderedTv = tvList.results.reduce(function (prev, item) {
                    return prev + that.renderListItem(item);
                }, '');

                renderedMovie = movieList.results.reduce(function (prev, item) {
                    return prev + that.renderListItem(item);
                }, '');

                if (tvList.page > 1) {
                    paginator += `<div class="prev" data-page="${tvList.page - 1}" title="previous">
                                  <i class="fa fa-chevron-circle-left"></i></div>`;
                }

                if (tvList.page < tvList.total_pages || movieList.page < movieList.total_pages) {
                    paginator += `<div class="next" data-page="${tvList.page + 1}" title="next">
                                  <i class="fa fa-chevron-circle-right"></i></div>`;
                }

                window.requestAnimationFrame(function () {
                    $('main').html(`<div class="movies">${renderedMovie}</div>
                                   <div class="tv-series">${renderedTv}</div>${paginator}`).scrollTop(0);
                });
            },
        },
    });

    window.App = App;

}(window.theMovieDb));
