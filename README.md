MovieDB-Testing-Challenge

# Overview
 * time constrain = 3-4 hours
 * simple application that should display search results for tv/movies from themoviedb
 * uses bare minimum for greater speed / lower page size
 * uses es6 template strings instead of template system (one page - really no need to use one)
 * does not use any dependency loader, injection etc. - it'd be overkill to use anything here.
 * styled by developer ... and designed by developer ...

# Installation
 * install dependencies `npm install --global gulp-cli bower`
 * install project `npm install .`
 * install front dependencies `bower install`
 * run `gulp serve` to preview and watch for changes
 * run `gulp` to build app for production
 * run `gulp serve:dist` to preview the production build


# TODO
 * decoupling main script for wrapper and data render
 * unit tests
 * prepare better layout
 * testing / fixes for browsers, atm tested in chrome ...
